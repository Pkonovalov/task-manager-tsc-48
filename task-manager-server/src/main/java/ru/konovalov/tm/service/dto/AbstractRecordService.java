package ru.konovalov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import ru.konovalov.tm.api.IRecordService;
import ru.konovalov.tm.api.service.IConnectionService;
import ru.konovalov.tm.dto.AbstractRecord;

public abstract class AbstractRecordService<E extends AbstractRecord> implements IRecordService<E> {

    @NotNull
    protected final IConnectionService connectionService;

    @NotNull
    public AbstractRecordService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

}
