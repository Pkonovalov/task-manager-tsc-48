package ru.konovalov.tm.api;


import ru.konovalov.tm.dto.AbstractRecord;

public interface IRecordService<E extends AbstractRecord> extends IRecordRepository<E> {

}