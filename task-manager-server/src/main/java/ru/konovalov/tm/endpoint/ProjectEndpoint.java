package ru.konovalov.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.konovalov.tm.api.service.ServiceLocator;
import ru.konovalov.tm.api.service.dto.IProjectRecordService;
import ru.konovalov.tm.api.service.dto.IProjectTaskRecordService;
import ru.konovalov.tm.api.service.model.IProjectService;
import ru.konovalov.tm.dto.ProjectRecord;
import ru.konovalov.tm.dto.SessionRecord;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;
import java.util.List;

@WebService
@NoArgsConstructor
public final class ProjectEndpoint extends AbstractEndpoint {

    private IProjectRecordService projectRecordService;

    private IProjectTaskRecordService ProjectTaskService;

    private IProjectService projectService;

    public ProjectEndpoint(
            @NotNull final ServiceLocator serviceLocator,
            @NotNull final IProjectRecordService projectRecordService,
            @NotNull final IProjectTaskRecordService ProjectTaskService,
            @NotNull final IProjectService projectService
    ) {
        super(serviceLocator);
        this.projectRecordService = projectRecordService;
        this.ProjectTaskService = ProjectTaskService;
        this.projectService = projectService;
    }

    @WebMethod
    public List<ProjectRecord> findProjectAll(@WebParam(name = "session") final SessionRecord session) {
        serviceLocator.getSessionRecordService().validate(session);
        List<ProjectRecord> list = projectRecordService.findAll(session.getUserId());
        return list;
    }

    @WebMethod
    public void addProjectAll(
            @WebParam(name = "session") final SessionRecord session,
            @WebParam(name = "collection") final Collection<ProjectRecord> collection
    ) {
        serviceLocator.getSessionRecordService().validate(session);
        projectRecordService.addAll(session.getUserId(), collection);
    }

    @WebMethod
    public ProjectRecord addProject(
            @WebParam(name = "session") final SessionRecord session, @WebParam(name = "entity") final ProjectRecord entity
    ) {
        serviceLocator.getSessionRecordService().validate(session);
        return projectRecordService.add(session.getUserId(), entity);
    }

    @WebMethod
    public ProjectRecord addProjectWithName(
            @WebParam(name = "session") final SessionRecord session,
            @WebParam(name = "name") String name,
            @WebParam(name = "description") String description
    ) {
        serviceLocator.getSessionRecordService().validate(session);
        return projectRecordService.add(session.getUserId(), name, description);
    }

    @WebMethod
    public ProjectRecord findProjectById(
            @WebParam(name = "session") final SessionRecord session, @WebParam(name = "id") final String id
    ) {
        serviceLocator.getSessionRecordService().validate(session);
        return projectRecordService.findById(session.getUserId(), id);
    }

    @WebMethod
    public void clearProject(@WebParam(name = "session") final SessionRecord session) {
        serviceLocator.getSessionRecordService().validate(session);
        projectService.clear(session.getUserId());
    }

    @WebMethod
    public void removeProject(
            @WebParam(name = "session") final SessionRecord session, @WebParam(name = "entity") final ProjectRecord entity
    ) {
        serviceLocator.getSessionRecordService().validate(session);
        projectRecordService.remove(session.getUserId(), entity);
    }

    @WebMethod
    public ProjectRecord findProjectByName(
            @WebParam(name = "session") final SessionRecord session, @WebParam(name = "name") final String name
    ) {
        serviceLocator.getSessionRecordService().validate(session);
        return projectRecordService.findByName(session.getUserId(), name);
    }

    @WebMethod
    public ProjectRecord findProjectByIndex(
            @WebParam(name = "session") final SessionRecord session, @WebParam(name = "index") final Integer index
    ) {
        serviceLocator.getSessionRecordService().validate(session);
        return projectRecordService.findByIndex(session.getUserId(), index);
    }

    @WebMethod
    public ProjectRecord updateProjectById(
            @WebParam(name = "session") final SessionRecord session,
            @WebParam(name = "id") final String id,
            @WebParam(name = "name") final String name,
            @WebParam(name = "description") final String description
    ) {
        serviceLocator.getSessionRecordService().validate(session);
        return projectRecordService.updateById(session.getUserId(), id, name, description);
    }

    @WebMethod
    public ProjectRecord updateProjectByIndex(
            @WebParam(name = "session") final SessionRecord session,
            @WebParam(name = "index") final Integer index,
            @WebParam(name = "name") final String name,
            @WebParam(name = "description") final String description
    ) {
        serviceLocator.getSessionRecordService().validate(session);
        return projectRecordService.updateByIndex(session.getUserId(), index, name, description);
    }

    @WebMethod
    public ProjectRecord startProjectById(
            @WebParam(name = "session") final SessionRecord session, @WebParam(name = "id") final String id
    ) {
        serviceLocator.getSessionRecordService().validate(session);
        return projectRecordService.startById(session.getUserId(), id);
    }

    @WebMethod
    public ProjectRecord startProjectByIndex(
            @WebParam(name = "session") final SessionRecord session, @WebParam(name = "index") final Integer index
    ) {
        serviceLocator.getSessionRecordService().validate(session);
        return projectRecordService.startByIndex(session.getUserId(), index);
    }

    @WebMethod
    public ProjectRecord startProjectByName(
            @WebParam(name = "session") final SessionRecord session, @WebParam(name = "name") final String name
    ) {
        serviceLocator.getSessionRecordService().validate(session);
        return projectRecordService.startByName(session.getUserId(), name);
    }

    @WebMethod
    public ProjectRecord finishProjectById(
            @WebParam(name = "session") final SessionRecord session, @WebParam(name = "id") final String id
    ) {
        serviceLocator.getSessionRecordService().validate(session);
        return projectRecordService.finishById(session.getUserId(), id);
    }

    @WebMethod
    public ProjectRecord finishProjectByIndex(
            @WebParam(name = "session") final SessionRecord session, @WebParam(name = "index") final Integer index
    ) {
        serviceLocator.getSessionRecordService().validate(session);
        return projectRecordService.finishByIndex(session.getUserId(), index);
    }

    @WebMethod
    public ProjectRecord finishProjectByName(
            @WebParam(name = "session") final SessionRecord session, @WebParam(name = "name") final String name
    ) {
        serviceLocator.getSessionRecordService().validate(session);
        return projectRecordService.finishByName(session.getUserId(), name);
    }

    @WebMethod
    public void removeProjectById(
            @WebParam(name = "session") final SessionRecord session, @WebParam(name = "projectId") final String projectId
    ) {
        serviceLocator.getSessionRecordService().validate(session);
        projectService.removeById(session.getUserId(), projectId);
    }

    @WebMethod
    public void removeProjectByIndex(
            @WebParam(name = "session") final SessionRecord session, @WebParam(name = "index") final Integer index
    ) {
        serviceLocator.getSessionRecordService().validate(session);
        projectRecordService.removeByIndex(session.getUserId(), index);
    }

    @WebMethod
    public void removeProjectByName(
            @WebParam(name = "session") final SessionRecord session, @WebParam(name = "name") final String name
    ) {
        serviceLocator.getSessionRecordService().validate(session);
        projectRecordService.removeByName(session.getUserId(), name);
    }
}
