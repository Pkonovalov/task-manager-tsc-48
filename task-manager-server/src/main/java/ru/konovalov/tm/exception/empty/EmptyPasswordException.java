package ru.konovalov.tm.exception.empty;

import org.jetbrains.annotations.NotNull;
import ru.konovalov.tm.exception.AbstractException;

public class EmptyPasswordException extends AbstractException {

    @NotNull
    public EmptyPasswordException() {
        super("Error. Password is empty");
    }

}
