package ru.konovalov.tm.exception.entity;

import org.jetbrains.annotations.NotNull;
import ru.konovalov.tm.exception.AbstractException;

public class UserNotFoundException extends AbstractException {

    @NotNull
    public UserNotFoundException() {
        super("Error. User not found.");
    }

}
