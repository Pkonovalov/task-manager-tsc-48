package ru.konovalov.tm.exception.empty;

import org.jetbrains.annotations.NotNull;
import ru.konovalov.tm.exception.AbstractException;

public class EmptyEmailException extends AbstractException {

    @NotNull
    public EmptyEmailException() {
        super("Error. Email is empty");
    }

}
