package ru.konovalov.tm.exception.entity;

import org.jetbrains.annotations.NotNull;
import ru.konovalov.tm.exception.AbstractException;

public class TaskNotFoundException extends AbstractException {

    @NotNull
    public TaskNotFoundException() {
        super("Error. Task not found.");
    }

}
