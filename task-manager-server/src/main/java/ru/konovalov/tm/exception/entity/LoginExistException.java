package ru.konovalov.tm.exception.entity;

import org.jetbrains.annotations.NotNull;
import ru.konovalov.tm.exception.AbstractException;

public class LoginExistException extends AbstractException {

    @NotNull
    public LoginExistException() {
        super("Error. Login already exist.");
    }

    @NotNull
    public LoginExistException(String value) {
        super("Error. Login '" + value + "' already exist.");
    }

}
