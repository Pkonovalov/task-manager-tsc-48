package ru.konovalov.tm.command.data;

import org.jetbrains.annotations.Nullable;
import ru.konovalov.tm.command.AuthAbstractCommand;

public class DataBinLoadCommand extends AuthAbstractCommand {

    @Nullable

    public String name() {
        return "data-load-bin";
    }

    @Nullable

    public String arg() {
        return null;
    }

    @Nullable

    public String description() {
        return "Load binary data";
    }

    public void execute() {
        serviceLocator.getDataEndpoint().loadDataBin(getSession());
    }

}