package ru.konovalov.tm.command.auth;

import org.jetbrains.annotations.Nullable;
import ru.konovalov.tm.command.AuthAbstractCommand;
import ru.konovalov.tm.util.TerminalUtil;

public class RegistryCommand extends AuthAbstractCommand {
    @Override
    public String name() {
        return "registry";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Create new user";
    }

    @Override
    public void execute() {
        System.out.println("Enter login");
        @Nullable final String login = TerminalUtil.nextLine();
        System.out.println("Enter password");
        @Nullable final String password = TerminalUtil.nextLine();
        System.out.println("Enter email");
        @Nullable final String email = TerminalUtil.nextLine();
        serviceLocator.getSessionEndpoint().register(login, password, email);
    }
}
