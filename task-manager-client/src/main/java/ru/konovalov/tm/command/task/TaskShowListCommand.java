package ru.konovalov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.konovalov.tm.command.TaskAbstractCommand;
import ru.konovalov.tm.endpoint.TaskDto;

import java.util.List;

public class TaskShowListCommand extends TaskAbstractCommand {
    @Override
    public String name() {
        return "task-list";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Show all tasks.";
    }

    @Override
    public void execute() {
        @Nullable List<TaskDto> tasks = serviceLocator.getTaskEndpoint().findTaskAll(getSession());
        int index = 1;
        for (@NotNull TaskDto task : tasks) {
            System.out.println(index + ". " + toString(task));
            index++;
        }
    }
}
