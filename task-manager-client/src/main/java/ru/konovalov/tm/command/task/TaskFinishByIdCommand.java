package ru.konovalov.tm.command.task;

import org.jetbrains.annotations.Nullable;
import ru.konovalov.tm.command.TaskAbstractCommand;
import ru.konovalov.tm.endpoint.TaskDto;
import ru.konovalov.tm.exception.entity.TaskNotFoundException;
import ru.konovalov.tm.util.TerminalUtil;

public class TaskFinishByIdCommand extends TaskAbstractCommand {
    @Override
    public String name() {
        return "task-finish-by-id";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Finish task by id.";
    }

    @Override
    public void execute() {
        System.out.println("Enter id");
        @Nullable final String id = TerminalUtil.nextLine();
        @Nullable final TaskDto task = serviceLocator.getTaskEndpoint().finishTaskById(getSession(), id);
        if (task == null) throw new TaskNotFoundException();
    }
}
